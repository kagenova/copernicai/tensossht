# %%
from datetime import datetime

import tensorflow as tf


class TransformBenchmark:
    def __init__(
        self,
        lmax: int = 100,
        is_real: bool = True,
        nbatches: int = 100,
        dtype=tf.float64,
    ):
        from tensossht.sampling import harmonic_sampling_scheme, image_sampling_scheme
        from tensossht.transforms.inverse import inverse_transform

        rdtype = tf.dtypes.as_dtype(dtype).real_dtype
        cdtype = tf.complex(tf.constant(0, rdtype), tf.constant(0, rdtype)).dtype

        self.hsampling = harmonic_sampling_scheme(lmax=100, mmax=0, spin=0)
        self.isampling = image_sampling_scheme("mw")
        self.transform = inverse_transform(
            self.hsampling, sampling=self.isampling, dtype=rdtype
        )

        size = (lmax * (lmax + 1)) // 2 if is_real else (lmax * lmax)
        self.coeffs = tf.squeeze(
            tf.cast(
                tf.random.uniform(shape=(nbatches, size, 2), dtype=cdtype.real_dtype),
                cdtype,
            )
            @ tf.constant([[1 + 0j], [1j]], dtype=cdtype)
        )

    def __call__(self, number: int = 1, repeat: int = 1):
        from timeit import Timer

        from tensossht.sampling import HarmonicAxes

        in_axes = HarmonicAxes(coeff=-1)
        fmm = self.transform.coeffs_to_fmm(self.coeffs, in_axes)
        overspectral = self.transform.fft(fmm)

        def timeit(stmt: str):
            return Timer(
                stmt=stmt,
                globals=dict(
                    self=self, in_axes=in_axes, fmm=fmm, overspectral=overspectral
                ),
            ).repeat(repeat, number)

        return dict(
            transform=timeit("self.transform(self.coeffs)"),
            coeffs_to_fmm=timeit("self.transform.coeffs_to_fmm(self.coeffs, in_axes)"),
            fft=timeit("self.transform.fft(fmm)"),
            slicer=timeit("self.transform.slicer(overspectral)"),
        )


# %%
class Coeffs2FmmBenchmark:
    def __init__(
        self,
        lmax: int = 100,
        is_real: bool = True,
        nbatches: int = 100,
        dtype=tf.float64,
    ):
        from tensossht.sampling import harmonic_sampling_scheme, image_sampling_scheme
        from tensossht.transforms.coeffs_to_fmm import coeffs_to_fmm

        rdtype = tf.dtypes.as_dtype(dtype).real_dtype
        cdtype = tf.complex(tf.constant(0, rdtype), tf.constant(0, rdtype)).dtype

        self.hsampling = harmonic_sampling_scheme(lmax=100, mmax=0, spin=0)
        self.isampling = image_sampling_scheme("mw")
        self.coeffs_to_fmm = coeffs_to_fmm(self.hsampling, self.isampling, rdtype)

        size = (lmax * (lmax + 1)) // 2 if is_real else (lmax * lmax)
        self.coeffs = tf.squeeze(
            tf.cast(
                tf.random.uniform(shape=(nbatches, size, 2), dtype=cdtype.real_dtype),
                cdtype,
            )
            @ tf.constant([[1 + 0j], [1j]], dtype=cdtype)
        )

    def __call__(self, number: int = 1, repeat: int = 1):
        from timeit import Timer

        from tensossht.sampling import HarmonicAxes

        in_axes = HarmonicAxes(coeff=-1)
        coeff_sum = self.coeffs_to_fmm.summation(self.coeffs)
        modulated = self._modulator(coeff_sum)

        def timeit(stmt: str):
            return Timer(
                stmt=stmt,
                globals=dict(
                    self=self, in_axes=in_axes, coeff_sum=coeff_sum, modulated=modulated
                ),
            ).repeat(repeat, number)

        return dict(
            total=timeit("self.coeffs_to_fmm(self.coeffs)"),
            summation=timeit("self.coeffs_to_fmm.summation(self.coeffs)"),
            modulation=timeit("self._modulator(coeff_sum)"),
            padding=timeit("self._padding(modulated)"),
        )

    @tf.function
    def _modulator(self, coeff_sum: tf.Tensor) -> tf.Tensor:
        perm = list(range(1, len(coeff_sum.shape) - 1)) + [0, len(coeff_sum.shape) - 1]
        return self.coeffs_to_fmm.modulator(tf.transpose(coeff_sum, perm=perm))

    @tf.function
    def _padding(self, modulated: tf.Tensor):
        return self.coeffs_to_fmm.padding(modulated)


class SummationBenchmark:
    def __init__(
        self,
        lmax: int = 100,
        is_real: bool = True,
        nbatches: int = 100,
        dtype=tf.float64,
    ):
        from tensossht.sampling import harmonic_sampling_scheme, image_sampling_scheme
        from tensossht.transforms.coeffs_to_fmm import coeffs_to_fmm

        rdtype = tf.dtypes.as_dtype(dtype).real_dtype
        cdtype = tf.complex(tf.constant(0, rdtype), tf.constant(0, rdtype)).dtype

        self.hsampling = harmonic_sampling_scheme(lmax=100, mmax=0, spin=0)
        self.isampling = image_sampling_scheme("mw")
        self.summation = coeffs_to_fmm(self.hsampling, self.isampling, rdtype).summation

        size = (lmax * (lmax + 1)) // 2 if is_real else (lmax * lmax)
        self.coeffs = tf.squeeze(
            tf.cast(
                tf.random.uniform(shape=(nbatches, size, 2), dtype=cdtype.real_dtype),
                cdtype,
            )
            @ tf.constant([[1 + 0j], [1j]], dtype=cdtype)
        )

    def __call__(self, number: int = 1, repeat: int = 1):
        from timeit import Timer

        def timeit(stmt: str):
            return Timer(
                stmt=stmt,
                globals=dict(self=self),
            ).repeat(repeat, number)

        return dict(
            total=timeit("self.summation(self.coeffs)"),
            iteration=timeit("self.iterators_only(self.coeffs)"),
            iteration_and_write=timeit("self.iterate_and_write(self.coeffs)"),
            calculations=timeit("self.calculate_and_write(self.coeffs)"),
            nolsum=timeit("self.nolsum(self.coeffs)"),
        )

    @tf.function
    def iterators_only(self, coefficients: tf.Tensor) -> tf.Tensor:
        assert coefficients.dtype in {tf.complex64, tf.complex128}
        self.summation.deltadelta_iterator.reset()

        state = self.summation.deltadelta_iterator.tf_state
        for _ in tf.range(self.summation.lmax - 1, -1, -1):
            _, state = self.summation.deltadelta_iterator.next(state)

    @tf.function
    def iterate_and_write(self, coefficients: tf.Tensor) -> tf.Tensor:
        assert coefficients.dtype in {tf.complex64, tf.complex128}
        self.summation.deltadelta_iterator.reset()

        state = self.summation.deltadelta_iterator.tf_state
        result = tf.TensorArray(coefficients.dtype, size=2 * self.summation.lmax - 1)
        dummy = tf.complex(
            tf.random.uniform(
                shape=(2 * self.summation.lmax - 1,),
                dtype=coefficients.dtype.real_dtype,
            ),
            tf.random.uniform(
                shape=(2 * self.summation.lmax - 1,),
                dtype=coefficients.dtype.real_dtype,
            ),
        )
        for mp in tf.range(self.summation.lmax - 1, -1, -1):
            _, state = self.summation.deltadelta_iterator.next(state)
            result = result.write(mp, dummy)
            if mp != 0:  # -mp branch
                result = result.write(2 * self.summation.lmax - 1 - mp, dummy)
        return result.stack()

    @tf.function
    def nolsum(self, coefficients: tf.Tensor) -> tf.Tensor:

        assert coefficients.dtype in {tf.complex64, tf.complex128}
        self.summation.deltadelta_iterator.reset()

        state = self.summation.deltadelta_iterator.tf_state
        for mp in tf.range(self.summation.lmax - 1, -1, -1):
            deltadelta, state = self.summation.deltadelta_iterator.next(state)
            deltas = tf.cast(
                deltadelta * self.summation.scale_factor, dtype=coefficients.dtype
            )
            summand = coefficients * deltas

            if mp != 0:  # -mp branch
                tf.where(self.summation.sign_condition, summand, -summand)

    @tf.function
    def calculations(self, coefficients: tf.Tensor) -> tf.Tensor:

        assert coefficients.dtype in {tf.complex64, tf.complex128}
        self.summation.deltadelta_iterator.reset()

        state = self.summation.deltadelta_iterator.tf_state
        for mp in tf.range(self.summation.lmax - 1, -1, -1):
            deltadelta, state = self.summation.deltadelta_iterator.next(state)
            deltas = tf.cast(
                deltadelta * self.summation.scale_factor, dtype=coefficients.dtype
            )
            summand = coefficients * deltas
            self.summation._lsum(summand)

            if mp != 0:  # -mp branch
                self.summation._lsum(
                    tf.where(self.summation.sign_condition, summand, -summand)
                )

    @tf.function
    def calculate_and_write(self, coefficients: tf.Tensor) -> tf.Tensor:

        assert coefficients.dtype in {tf.complex64, tf.complex128}
        self.summation.deltadelta_iterator.reset()

        state = self.summation.deltadelta_iterator.tf_state
        result = tf.TensorArray(coefficients.dtype, size=2 * self.summation.lmax - 1)
        for mp in tf.range(self.summation.lmax - 1, -1, -1):
            deltadelta, state = self.summation.deltadelta_iterator.next(state)
            deltas = tf.cast(
                deltadelta * self.summation.scale_factor, dtype=coefficients.dtype
            )
            summand = coefficients * deltas
            a = self.summation._lsum(summand)
            result = result.write(mp, a)

            if mp != 0:  # -mp branch
                b = self.summation._lsum(
                    tf.where(self.summation.sign_condition, summand, -summand)
                )
                result = result.write(2 * self.summation.lmax - 1 - mp, b)
        return result.stack()


# %%
tranform = TransformBenchmark(is_real=True)
coeffs2fmm = Coeffs2FmmBenchmark(is_real=True)
summation = SummationBenchmark(is_real=True)

# %%
stamp = datetime.now().strftime("%Y%m%d-%H%M%S")
logdir = "logs/func/%s" % stamp
writer = tf.summary.create_file_writer(logdir)

# %%
tf.summary.trace_on(graph=True, profiler=True)
summation.calculations(summation.coeffs)
with writer.as_default():
    tf.summary.trace_export(name="without_write", step=0, profiler_outdir=logdir)

# %%
tf.summary.trace_on(graph=True, profiler=True)
summation.calculate_and_write(summation.coeffs)
with writer.as_default():
    tf.summary.trace_export(name="with_write", step=0, profiler_outdir=logdir)

# %%
