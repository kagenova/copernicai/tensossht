[docs-img]: https://img.shields.io/badge/docs-stable-blue.svg
[docs-url]: https://tensossht.readthedocs.io/en/main/

[pipeline-url]: https://gitlab.com/kagenova/copernicai/tensossht/-/commits/main
[pipeline-image]: https://gitlab.com/kagenova/copernicai/tensossht/badges/main/pipeline.svg

[license-image]: https://img.shields.io/badge/license-Apache--2-blue
[license-url]: https://gitlab.com/kagenova/copernicai/tensossht/-/blob/main/LICENSE

[![][docs-img]][docs-url]
[![][pipeline-image]][pipeline-url]
[![][license-image]][license-url]


TensoSSHT
=========

TensoSSHT provides exact and differentiable spherical harmonic transforms for
[TensorFlow](https://www.tensorflow.org/).  In particular, Fourier transforms for
signals on the sphere $\mathbb{S}^2$ and rotation group $\text{SO}(3)$, i.e. spherical
harmonic and Wigner transforms, respectively, are supported.

TensoSSHT is a TensorFlow implementation of the
[SSHT](https://github.com/astro-informatics/ssht) and
[SO3](https://github.com/astro-informatics/so3) codes, implementing the transforms
presented in [McEwen & Wiaux (2011)](https://arxiv.org/abs/1110.6298) and [McEwen et al.
(2015)](https://arxiv.org/abs/1508.03101), respectively.  The code was developed at
[Kagenova](https://www.kagenova.com/).

TensoSSHT is distributed under the Apache License 2.0.


Usage in a nutshell
-------------------

 Usage is as simple as:

```python
import tensorflow as tf
from tensossht import harmonic_transform
from pytest import approx

# create an object with a forward (image to flm) and inverse transform (flm to image)
transform = harmonic_transform(lmax=64, spin=0, dtype=tf.complex128)

# create a random array with batch size 15
shape = 15, transform.harmonic_sampling.ncoeffs
flm_coefficients = tf.complex(
    tf.random.uniform(shape, dtype=transform.real_dtype),
    tf.random.uniform(shape, dtype=transform.real_dtype),
)

images = transform.inverse(flm_coefficients)
assert transform.forward(images).numpy() == approx(flm_coefficients.numpy())
```

Layers for tensorflow are also available `tensossht.layers` including layers for the
inverse and forward Wigner transforms. For more information, please check the
[documentation](https://tensossht.readthedocs.io/en/main/layers.html).


Installation
------------

TensoSSHT is released on [pypi](https://pypi.org/project/tensossht/) and can be
installed via `pip`:

```bash
> pip install tensossht
```

It is also possible to install it from source. TensoSSHT is developed with
[poetry](https://python-poetry.org/).
and run:

```bash
> git clone https://gitlab.com/kagenova/copernicai/tensossht
> cd tensossht
> poetry install --with=docs
> poetry run pytest  # Optional, all tests should pass :)
> poetry run python
```

The `--with=docs` also installs the dependencies for building the documentation and is not
strictly necessary otherwise.


Building the docs
-----------------

Tensossht contains fairly extensive documentation. The documentation  is available
[on-line](https://tensossht.readthedocs.io/en/main.html). The examples are exercised
when running the test suite, and hence should be at least usable. To build the
documentation, do:

```bash
poetry run sphinx-build docs build/html
```

Then open the file `build/html/index.html`.

Development
-----------

The code is formatted using [black](https://black.readthedocs.io/en/stable/) and
[isort](https://pycqa.github.io/isort/). Pre-commit hooks can be installed via
[pre-commit](https://pre-commit.com/).

A Note about Performance
========================

As of tensorflow 2.11, we've had varying success running the forward and inverse
transforms depending on hardware. On CPUs the inverse and forward transform perform more
or less the same, with the latter having a slight edge. On GPUs however, the inverse
transform can be quite a bit slower, especially at large L.

Referencing
-----------

If you use TensoSSHT for work that results in publication, please reference the code itself and also our related academic papers:
- J. D. McEwen and Y. Wiaux, A novel sampling theorem on the sphere, IEEE Trans. Sig. Proc., 59(12):5876-5887, 2011 ([arXiv:1110.6298](https://arxiv.org/abs/1110.6298)).
- J. D. McEwen, M. Büttner, B. Leistedt, H. V. Peiris, Y. Wiaux, A novel sampling theorem on the rotation group, IEEE Sig. Proc. Let., 22(12):2425-2429, 2015 ([arXiv:1508.03101](https://arxiv.org/abs/1508.03101))
