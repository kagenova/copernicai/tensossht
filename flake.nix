{
  description = "TensoSSHT environment";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs @ {
    self,
    devshell,
    nixpkgs,
    ...
  }: let
    system = "x86_64-darwin";

    pkgs = import nixpkgs rec {
      inherit system;
      overlays = [devshell.overlay];
    };
  in {
    devShell.${system} = pkgs.devshell.mkShell {
      devshell.name = "TensoSSHT";
      commands = [
        {package = "poetry";}
        {package = "python38";}
        {package = "graphviz";}
        {package = pkgs.texlive.combined.scheme-full;}
      ];
    };
  };
}
