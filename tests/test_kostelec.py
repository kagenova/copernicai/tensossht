import numpy as np
import tensorflow as tf
from pytest import approx, mark


def test_dll0():
    from tensossht.sampling import legendre_labels
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import numericalrecipes as nr

    lmax = 8
    beta = tf.constant(
        [0, np.pi / 3, np.pi / 2, 2 * np.pi / 3, np.pi], dtype=tf.float64
    )
    x = tf.cos(beta)
    labels = legendre_labels(lmax=lmax, mmin=0)
    actual = tf.where(labels[0] == labels[1], kos.legendre(beta=beta, lmax=lmax), 0)
    expected = tf.where(
        labels[0] == labels[1], nr.legendre(lmax=lmax, x=x, scaled=True), 0
    )
    assert actual.numpy() == approx(expected.numpy())


def test_dllm10():
    from tensossht.sampling import legendre_labels
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import numericalrecipes as nr

    lmax = 8
    beta = tf.constant(
        [0, np.pi / 3, np.pi / 2, 2 * np.pi / 3, np.pi], dtype=tf.float64
    )
    x = tf.cos(beta)
    labels = legendre_labels(lmax=lmax, mmin=0)
    actual = tf.where(labels[0] == labels[1] + 1, kos.legendre(beta=beta, lmax=lmax), 0)
    expected = tf.where(
        labels[0] == labels[1] + 1, nr.legendre(lmax=lmax, x=x, scaled=True), 0
    )
    assert actual.numpy() == approx(expected.numpy())


def test_legendre():
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import numericalrecipes as nr

    lmax = 8
    beta = tf.constant(
        [0, np.pi / 3, np.pi / 2, 2 * np.pi / 3, np.pi], dtype=tf.float64
    )
    x = tf.cos(beta)
    actual = kos.legendre(beta=beta, lmax=lmax)
    expected = nr.legendre(lmax=lmax, x=x, scaled=True)
    assert actual.numpy() == approx(expected.numpy())


def test_legendre_bad_label():
    from pytest import approx

    from tensossht.specialfunctions import kostelec as kos

    betas = tf.constant([np.pi / 2])
    labels = tf.constant([[0, 1, 0], [0, 2, 1]])
    #
    actuals = kos.legendre(beta=betas, labels=labels).numpy()

    assert actuals[0, 0] == approx(1.0)
    assert actuals[0, 1] == approx(0.0)
    assert actuals[0, 2] == approx(0.0)


def test_legendre_negative_m():
    from tensossht.sampling import legendre_labels
    from tensossht.specialfunctions import kostelec as kos

    lmax = 8
    beta = tf.constant(
        [0, np.pi / 3, np.pi / 2, 2 * np.pi / 3, np.pi], dtype=tf.float64
    )
    labels = legendre_labels(lmax=lmax)
    positive = kos.legendre(beta=beta, labels=labels)
    negative = kos.legendre(beta=beta, labels=tf.concat((labels[:1], -labels[1:]), 0))
    factors = tf.cast(tf.where(labels[1] % 2 == 1, -1, 1), dtype=beta.dtype)
    assert negative.numpy() == approx((positive * factors).numpy())


def test_kostelec_with_partial_vector():
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import numericalrecipes as nr

    lmax = 8
    lmin = 2
    mmax = 4
    mmin = 2
    beta = tf.constant([np.pi / 3, np.pi / 2, 2 * np.pi / 3], dtype=tf.float64)
    x = tf.cos(beta)
    actual = kos.legendre(beta=beta, lmax=lmax, lmin=lmin, mmax=mmax, mmin=mmin)
    expected = nr.legendre(lmax=lmax, x=x, scaled=True, lmin=lmin, mmax=mmax, mmin=mmin)
    assert actual.numpy() == approx(expected.numpy())


@mark.parametrize("beta", [0, np.pi / 5, np.pi / 2, np.pi / 3 * 2, np.pi])
def test_symmetries(beta):
    from tensossht.sampling import wignerd_labels
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import naive

    labels = wignerd_labels(4)
    relabeled, factors = kos._symmetries(labels)
    assert relabeled.shape == labels.shape
    assert factors.shape[0] == relabeled.shape[1]
    for i in range(labels.shape[1]):
        original = naive.wignerd(*labels[:, i].numpy().tolist(), beta)  # type: ignore
        sym = naive.wignerd(*relabeled[:, i].numpy().tolist(), beta)  # type: ignore
        assert original == (-sym if factors[i] == -1 else sym)


@mark.parametrize("beta", [0, np.pi / 5, np.pi / 2, np.pi / 3 * 2, np.pi])
def test_dllm(beta):
    from tensossht.sampling import wignerd_labels
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import naive

    lmax = 8
    labels = wignerd_labels(lmax=lmax).numpy().T.tolist()
    actuals = kos.wignerd(beta=tf.constant(beta, dtype=tf.float64), lmax=lmax).numpy().T

    for ((l, m, mp), actual) in zip(labels, actuals):
        if l != abs(m) or l != abs(mp):
            continue
        expected = naive.wignerd(l, m, mp, beta=beta)
        assert actual == approx(float(expected))


@mark.parametrize("beta", [0, np.pi / 5, np.pi / 2, np.pi / 3 * 2, np.pi])
def test_dllm1m(beta):
    from tensossht.sampling import wignerd_labels
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import naive

    lmax = 8
    labels = wignerd_labels(lmax=lmax).numpy().T.tolist()
    actuals = kos.wignerd(beta=tf.constant(beta, dtype=tf.float64), lmax=lmax).numpy().T

    for ((l, m, mp), actual) in zip(labels, actuals):
        if l != max(abs(m), abs(mp)) + 1 and l != max(abs(m), abs(mp)):
            continue
        expected = naive.wignerd(l, m, mp, beta=beta)
        assert actual == approx(float(expected))


def test_wignerd():
    from tensossht.sampling import wignerd_labels
    from tensossht.specialfunctions import kostelec as kos
    from tensossht.specialfunctions import naive

    lmax = 8
    betas = tf.constant(
        [0, np.pi / 5, np.pi / 2, np.pi / 3 * 2, np.pi], dtype=tf.float64
    )
    labels = wignerd_labels(lmax=lmax).numpy().T.tolist()
    actuals = kos.wignerd(beta=betas, lmax=lmax).numpy().T

    for ((l, m, mp), actual_betas) in zip(labels, actuals):
        for beta, actual in zip(betas, actual_betas):
            expected = naive.wignerd(l, m, mp, beta=float(beta))
            assert actual == approx(float(expected))


def test_wignerd_bad_label():
    from pytest import approx

    from tensossht.specialfunctions import kostelec as kos

    betas = tf.constant([np.pi / 2])
    labels = tf.constant([[0, 1, 1, 0], [0, 2, 0, 0], [0, 0, 2, 4]])
    #
    actuals = kos.wignerd(beta=betas, labels=labels).numpy()

    assert actuals[0, 0] == approx(1.0)
    assert actuals[0, 1] == approx(0.0)
    assert actuals[0, 2] == approx(0.0)
    assert actuals[0, 3] == approx(0.0)
