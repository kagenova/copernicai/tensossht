import numpy as np
import tensorflow as tf
from pytest import approx, mark


@mark.parametrize("sampling", ["mw", "mwss"])
def test_images_to_fmm_real(sampling, lmax=5):
    from tensossht import spherical_harmonics
    from tensossht.sampling import (
        ImageAxes,
        harmonic_sampling_scheme,
        image_sampling_scheme,
    )
    from tensossht.transforms.images_to_fmm import images_to_fmm

    grid = image_sampling_scheme(sampling).value(lmax, dtype=tf.float64).grid
    sph = spherical_harmonics(grid[0], grid[1], labels=[[0, 1], [0, 1]])

    hsampling = harmonic_sampling_scheme(lmax, mmin=0, spin=0)
    itof = images_to_fmm(hsampling, sampling, dtype=grid.dtype)
    starting_axes = ImageAxes(theta=0, phi=1)
    fmm = itof(tf.math.real(sph), starting_axes)
    assert fmm[0, 0, lmax - 1].numpy() == approx(0.5 / np.sqrt(np.pi))
    assert tf.reduce_sum(tf.abs(fmm[0])).numpy() == approx(0.5 / np.sqrt(np.pi))

    f11 = fmm[1, 1, lmax].numpy()
    assert fmm[1, 1, lmax - 2].numpy() == approx(-f11)
    assert tf.reduce_sum(tf.abs(fmm[1])).numpy() == approx(2 * np.abs(f11))


@mark.parametrize("sampling", ["mw", "mwss"])
def test_images_to_fmm_complex(sampling, lmax=5):
    from tensossht import spherical_harmonics
    from tensossht.sampling import (
        ImageAxes,
        harmonic_sampling_scheme,
        image_sampling_scheme,
    )
    from tensossht.transforms.images_to_fmm import images_to_fmm

    grid = image_sampling_scheme(sampling).value(lmax, dtype=tf.float64).grid
    sph = spherical_harmonics(grid[0], grid[1], labels=[[0, 1], [0, 1]])

    hsampling = harmonic_sampling_scheme(lmax, mmin=None, spin=0)
    itof = images_to_fmm(hsampling, sampling, dtype=grid.dtype)
    starting_axes = ImageAxes(theta=0, phi=1)
    fmm = itof(sph, starting_axes)
    assert fmm[0, lmax - 1, lmax - 1].numpy() == approx(0.5 / np.sqrt(np.pi))
    assert tf.reduce_sum(tf.abs(fmm[0])).numpy() == approx(0.5 / np.sqrt(np.pi))

    f11 = fmm[1, lmax, lmax].numpy()
    assert fmm[1, lmax, lmax - 2].numpy() == approx(-f11)
    assert tf.reduce_sum(tf.abs(fmm[1])).numpy() == approx(2 * np.abs(f11))


def test_weights():
    from numpy import pi

    from tensossht.transforms.fmm_to_gmm import weights

    lmax = 5
    w = weights(lmax)
    assert w[2 * lmax - 1] == approx(-pi / 2 * 1j)
    assert w[2 * lmax - 3] == approx(pi / 2 * 1j)
    w[[2 * lmax - 1, 2 * lmax - 3]] = 0
    assert w[1::2] == approx(0)
    assert w[0] == 2 / (1 - (2 * lmax - 2) ** 2)
    assert w.shape == (4 * lmax - 3,)


@mark.parametrize("lmax, ndim", [(1, 2), (5, 2), (5, 3)])
def test_convolution(lmax, ndim):
    from tensossht.transforms.fmm_to_gmm import ComplexFmmToGmm

    def weight(m: int) -> complex:
        if abs(m) == 1:
            return -np.sign(m) * np.pi / 2 * 1j
        elif m % 2 == 0:
            return 2 / (1 - m * m)
        return 0

    L = lmax - 1
    shape = np.random.randint(2, 5, max(ndim - 2, 0)).tolist() + [lmax, 2 * L + 1]
    functions = tf.complex(
        tf.random.uniform(shape, dtype=tf.float64),
        tf.random.uniform(shape, dtype=tf.float64),
    )

    convolver = ComplexFmmToGmm(lmax, tf.complex128)
    convolved = convolver(functions)
    gmm = np.zeros((*functions.shape[:-1], 4 * L + 1), dtype=convolved.numpy().dtype)
    padded = np.zeros((*functions.shape[:-1], 4 * L + 1), dtype=functions.numpy().dtype)
    padded[..., L : 3 * L + 1] = functions
    for j in range(-2 * L, 2 * L + 1):
        for i in range(-2 * L, 2 * L + 1):
            gmm[..., j + 2 * L] += padded[..., i + 2 * L] * weight(j - i)
            if i == 6:
                print(gmm[0].round(2))

    assert convolved.numpy() == approx(gmm[..., L : 3 * L + 1])


def test_gmm_to_flm(lmax=4, dtype=tf.float64):
    from tensossht.sampling import spin_legendre_labels
    from tensossht.specialfunctions.naive import wignerd
    from tensossht.transforms.gmm_to_coeffs import gmm_to_coeffs

    gmm = tf.complex(
        tf.ones((3, 2 * lmax - 1, 2 * lmax - 1), dtype=dtype),
        tf.ones((3, 2 * lmax - 1, 2 * lmax - 1), dtype=dtype),
    )

    labels = spin_legendre_labels(lmax, smin=-lmax, smax=lmax)
    transform = gmm_to_coeffs(labels, dtype=dtype)
    result = transform(gmm)

    for i, (l, m, spin) in enumerate(labels.numpy().T):
        expected = (
            np.array(
                [
                    float(wignerd(l, mp, m) * wignerd(l, mp, -spin))
                    * gmm[..., m + lmax - 1, mp + lmax - 1].numpy()
                    for mp in range(-l, l + 1)
                ]
            )
            * ((-1) ** spin if spin >= 0 else 1 / (-1) ** (-spin))
            * (1j ** (m + spin) if m + spin >= 0 else 1 / 1j ** (-m - spin))
            * np.sqrt((2 * l + 1) * np.pi)
        )
        assert result[..., i].numpy() == approx(np.sum(expected, axis=0))


@mark.parametrize("spin", list(range(4)))
@mark.parametrize("sampling", ["mw", "mwss"])
def test_complex_forward(sampling, spin, lmax=5, dtype=tf.float64):
    from tensossht.sampling import image_sampling_scheme
    from tensossht.specialfunctions.naive import spin_spherical_harmonics
    from tensossht.transforms.forward import forward_transform

    transform = forward_transform(lmax, dtype=dtype, sampling=sampling, spin=spin)
    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = spin_spherical_harmonics(
        grid[0], grid[1], lmax=lmax, mmin=None, smin=spin, smax=spin
    )

    sph_space = transform(sph, theta_dim=0, phi_dim=1).numpy()
    assert sph_space == approx(tf.eye(sph.shape[-1]).numpy(), abs=1e-7)


@mark.parametrize("sampling", ["mw", "mwss"])
def test_real_forward(sampling, lmax=5, dtype=tf.float64):
    from tensossht import spherical_harmonics
    from tensossht.sampling import image_sampling_scheme, legendre_labels
    from tensossht.transforms.forward import forward_transform

    transform = forward_transform(lmax, mmin=0, spin=0, dtype=dtype, sampling=sampling)
    labels = legendre_labels(lmax, mmin=0)

    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = spherical_harmonics(grid[0], grid[1], labels=labels)
    rsph = tf.math.real(sph)

    sph_space = transform(rsph, theta_dim=0, phi_dim=1).numpy()
    expected = tf.eye(sph.shape[-1], dtype=tf.int32) / tf.where(labels[1] == 0, 1, 2)
    assert sph_space == approx(expected.numpy(), abs=1e-7)


def ensure_second_call_works(sampling="mw", lmax=5, dtype=tf.float64):
    from tensossht import spherical_harmonics
    from tensossht.sampling import image_sampling_scheme, legendre_labels
    from tensossht.transforms.forward import forward_transform

    transform = forward_transform(lmax, mmin=0, spin=0, dtype=dtype, sampling=sampling)
    labels = legendre_labels(lmax, mmin=0)

    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = spherical_harmonics(grid[0], grid[1], labels=labels)
    rsph = tf.math.real(sph)

    first = transform(rsph, theta_dim=0, phi_dim=1).numpy()
    second = transform(2 * rsph, theta_dim=0, phi_dim=1).numpy()
    assert 2 * first == approx(second, abs=1e-7)


def test_complex_spin_forward(sampling="mw", smin=-2, smax=2, lmax=5, dtype=tf.float64):
    from tensossht.sampling import image_sampling_scheme
    from tensossht.specialfunctions.naive import spin_spherical_harmonics
    from tensossht.transforms.forward import forward_transform

    transform = forward_transform(
        lmax, smin=smin, smax=smax, dtype=dtype, sampling=sampling, compact_spin=False
    )
    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = spin_spherical_harmonics(
        grid[0], grid[1], lmax=lmax, mmin=None, smin=smin, smax=smax, compact_spin=False
    )
    sph = tf.reshape(sph, (sph.shape[0], sph.shape[1], smax - smin + 1, -1))

    sph_space = transform(sph, theta_dim=0, phi_dim=1, spin_dim=2).numpy()

    for s in tf.range(smin, smax + 1):
        expected = np.eye(sph_space.shape[-1])
        expected[: s * s, : s * s] = 0
        assert sph_space[:, s - smin] == approx(expected, abs=1e-6, rel=1e-6)
