from typing import Any, cast

import numpy as np
import tensorflow as tf
from pytest import approx, mark


def test_iterator(lmax=8):
    from tensossht import wignerd_labels
    from tensossht.iterators import TrapaniIterator
    from tensossht.specialfunctions.naive import wignerd

    iterator = TrapaniIterator.factory(
        lmax=lmax, mmin=0, mmax=0, mpmax=lmax, mpmin=0, dtype=tf.float64
    )
    labels = wignerd_labels(lmax=lmax, mmax=0, mmin=0, mpmax=lmax, mpmin=0)
    assert tf.reduce_all(iterator.llabels == labels[0])

    for actual, m in zip(iterator, range(lmax - 1, -1, -1)):
        expected = np.array(
            [float(wignerd(l, m, mp)) for (l, mp) in np.array(labels[::2]).T]
        )
        assert np.array(actual) == approx(expected)

    assert np.array(next(iterator)) == approx(0)
    assert np.array(next(iterator)) == approx(0)
    assert np.array(next(iterator)) == approx(0)


def test_multispin_deltadeltaiterator(lmax=8, smax=3, smin=-3, dtype=tf.float64):
    from tensossht import spin_legendre_labels
    from tensossht.iterators import DeltaDeltaIterator, TrapaniIterator
    from tensossht.specialfunctions.trapani import deltas

    sqrts = tf.sqrt(tf.cast(tf.range(2 * (lmax + 1) + 1), dtype=dtype))
    llm_labels = spin_legendre_labels(lmax, mmin=None, smax=0, smin=0)
    llm_data = deltas(
        labels=tf.concat((llm_labels[:1], llm_labels[:1], llm_labels[1:2]), axis=0),
        dtype=dtype,
    )
    llm_iter = TrapaniIterator(llm_labels[:2], llm_data, sqrts)
    lls_labels = spin_legendre_labels(
        lmax, mmin=None, smax=smax, smin=smin, compact_spin=False
    )

    lls_data = deltas(
        labels=tf.concat((lls_labels[:1], lls_labels[:1], -lls_labels[2:3]), axis=0),
        dtype=dtype,
    )
    lls_iter = TrapaniIterator(
        tf.concat((lls_labels[:1], -lls_labels[2:3]), axis=0), lls_data, sqrts
    )

    dd_iter = DeltaDeltaIterator.factory(lls_labels, dtype=dtype)

    state = dd_iter.tf_state
    for _, llm, lls in zip(range(lmax, -1, -1), llm_iter, lls_iter):
        dd, state = cast(Any, dd_iter.next(state))
        assert dd.shape == (smax - smin + 1, len(llm))
        invalid_is_zero = tf.where(
            tf.abs(lls_labels[2]) > lls_labels[0], tf.reshape(dd, (-1,)) == 0, True
        )
        assert tf.reduce_all(invalid_is_zero)
        expected = tf.where(
            tf.reshape(tf.abs(lls_labels[2]) > lls_labels[0], (-1, len(llm))),
            0,
            llm * tf.reshape(lls, (-1, len(llm))),
        )
        assert dd.numpy() == approx(expected.numpy())


def test_literator_ll0():
    from tensossht.iterators import LIterator
    from tensossht.specialfunctions.trapani import straightforward

    iterator = LIterator(tf.float64)
    assert np.array(iterator._ll0) == approx(straightforward(0, 0, 0))
    assert np.array(iterator._next_ll0(1)) == approx(straightforward(1, 1, 0))
    assert np.array(iterator._next_ll0(2)) == approx(straightforward(2, 2, 0))
    assert np.array(iterator._next_ll0(3)) == approx(straightforward(3, 3, 0))


def test_literator_llm():
    from tensossht.iterators import LIterator
    from tensossht.specialfunctions.trapani import straightforward

    def expected(degree):
        return [straightforward(degree, degree, i) for i in range(1, degree + 1)]

    iterator = LIterator(tf.float64)
    assert np.array(iterator._next_llm(1, iterator._ll0)) == approx(expected(1))
    assert np.array(iterator._next_llm(2, iterator._next_ll0(1))) == approx(expected(2))
    assert np.array(iterator._next_llm(3, iterator._next_ll0(2))) == approx(expected(3))


def test_literator_lmpm():
    from tensossht.iterators import LIterator
    from tensossht.specialfunctions.trapani import straightforward

    def expected(degree):
        return [
            straightforward(degree, m1, m2)
            for m1 in range(0, degree)
            for m2 in range(0, m1 + 1)
        ]

    iterator = LIterator(tf.float64)
    ll0s = tf.stack(
        [iterator._ll0] + [iterator._next_ll0(i) for i in range(1, 4)], axis=0
    )
    llm = iterator._next_llm(1, ll0s[0])
    assert np.array(iterator._next_lmpm(1, ll0s[1], llm)) == approx(expected(1))
    llm = iterator._next_llm(2, ll0s[1])
    assert np.array(iterator._next_lmpm(2, ll0s[2], llm)) == approx(expected(2))
    llm = iterator._next_llm(3, ll0s[2])
    assert np.array(iterator._next_lmpm(3, ll0s[3], llm)) == approx(expected(3))


def test_literator():
    from tensossht.iterators import LIterator
    from tensossht.specialfunctions.trapani import straightforward

    def expected(degree):
        return [
            straightforward(degree, m1, m2)
            for m1 in range(0, degree + 1)
            for m2 in range(0, m1 + 1)
        ]

    for degree, actual in enumerate(LIterator(max_degree=10, dtype=tf.float64)):
        assert np.array(actual) == approx(expected(degree))


@mark.parametrize("spin", list(range(-6, 6)))
def test_ldeltadelta_iterator(spin, max_degree=10):
    from tensossht.iterators import LDeltaDeltaIterator
    from tensossht.specialfunctions.trapani import straightforward

    def expected(degree):
        return [
            straightforward(degree, m1, m2) * straightforward(degree, m1, -spin)
            for m1 in range(0, degree + 1)
            for m2 in range(0, m1 + 1)
        ]

    iterator = LDeltaDeltaIterator(max_degree=max_degree, dtype=tf.float64, spin=spin)
    for degree, actual in enumerate(iterator, start=abs(spin)):
        assert np.array(actual) == approx(expected(degree)), degree


def test_literator_symmetrize(max_degree=10):
    from tensossht.iterators import LIterator
    from tensossht.specialfunctions.trapani import straightforward

    def expected(degree):
        return np.array(
            [
                [straightforward(degree, m1, m2) for m1 in range(-degree, degree + 1)]
                for m2 in range(-degree, degree + 1)
            ]
        )

    iterator = LIterator(max_degree=max_degree, dtype=tf.float64)
    for degree, compact in enumerate(iterator):
        actual = LIterator.symmetrize(degree, compact)
        assert np.array(actual) == approx(expected(degree)), degree
