import numpy as np
import tensorflow as tf
from pytest import approx, mark


def test_l2():
    from tensorflow import tensordot

    from tensossht.specialfunctions.naive import wignerd as naive
    from tensossht.specialfunctions.risbo import recurrence

    R1 = recurrence(1, beta=np.pi / 7)
    R2 = recurrence(2, beta=np.pi / 7)
    wignerd = tensordot(R1, R2, axes=[[0, 1], [2, 3]])
    assert np.array(wignerd) == approx(np.array(naive(2, beta=np.pi / 7), dtype=float))


def test_l3():
    from tensossht.specialfunctions.naive import wignerd as naive
    from tensossht.specialfunctions.risbo import recurrence

    R1 = recurrence(1, beta=np.pi / 7)
    R2 = recurrence(2, beta=np.pi / 7)
    R3 = recurrence(3, beta=np.pi / 7)
    wignerd = tf.tensordot(
        R3, tf.tensordot(R2, R1, axes=[[2, 3], [0, 1]]), axes=[[2, 3], [0, 1]]
    )
    assert np.array(wignerd) == approx(np.array(naive(3, beta=np.pi / 7), dtype=float))


def test_legendre():
    from tensossht.specialfunctions.naive import wignerd as naive
    from tensossht.specialfunctions.risbo import wignerd

    beta = tf.constant(np.pi / 7, dtype=tf.float32)
    l3 = wignerd(3, beta)
    assert np.array(l3) == approx(naive(3, beta=np.pi / 7).astype(dtype=float))


@mark.parametrize("l", [0] + np.random.randint(1, 5, (10,)).tolist())
def test_straightforward(l):
    from tensossht.specialfunctions import naive, risbo

    beta = 2 * np.random.random() - 1.0
    assert risbo.straightforward(l, l + 1, 0, beta) == approx(0)
    assert risbo.straightforward(l, -l - 1, 0, beta) == approx(0)
    assert risbo.straightforward(l, 0, l + 1, beta) == approx(0)
    assert risbo.straightforward(l, 0, -l - 1, beta) == approx(0)

    args = l, *(np.random.randint(-l, l, 2) if l > 0 else (0, 0)), beta
    assert risbo.straightforward(*args) == approx(naive.wignerd(*args))  # type: ignore


@mark.parametrize("l", [0, 1, 2, 3])
def test_degree(l):
    from tensossht.specialfunctions import naive, risbo

    beta = 2 * np.random.random(2) - 1.0
    expected = np.array(
        [
            [
                [float(naive.wignerd(l, i, j, k)) for j in range(-l, l + 1)]
                for i in range(-l, l + 1)
            ]
            for k in beta
        ],
        dtype="float32",
    )

    assert risbo.degree(l, beta)[-1].numpy() == approx(expected)
