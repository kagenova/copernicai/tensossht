import tensorflow as tf
from pytest import approx, mark


@mark.parametrize("sampling", (("mw", "mwss")))
def test_back_and_forth_complex(sampling, lmax=8, dtype=tf.complex128):
    from tensossht.transforms.transforms import harmonic_transform

    transform = harmonic_transform(
        lmax, spin=0, mmin=-lmax, sampling=sampling, dtype=dtype
    )
    images = tf.complex(
        tf.random.uniform((2, *transform.sampling.shape), dtype=transform.real_dtype),
        tf.random.uniform((2, *transform.sampling.shape), dtype=transform.real_dtype),
    )
    representable_images = transform.inverse(transform.forward(images))
    assert tf.reduce_any(representable_images != 0)

    toandfro = transform.inverse(transform.forward(representable_images))
    assert toandfro.numpy() == approx(representable_images.numpy())


@mark.parametrize("sampling", (("mw", "mwss")))
def test_back_and_forth_real(sampling, lmax=8, dtype=tf.complex128):
    from tensossht.transforms.transforms import harmonic_transform

    transform = harmonic_transform(lmax, spin=0, mmin=0, sampling=sampling, dtype=dtype)
    images = tf.random.uniform(
        (2, *transform.sampling.shape), dtype=transform.real_dtype
    )
    representable_images = transform.inverse(transform.forward(images))
    assert tf.reduce_any(representable_images != 0)

    toandfro = transform.inverse(transform.forward(representable_images))
    assert toandfro.numpy() == approx(representable_images.numpy())
