from pytest import fixture


@fixture(autouse=True)
def warnings_as_errors(request):
    from warnings import simplefilter

    simplefilter("error", FutureWarning)
    simplefilter("error", PendingDeprecationWarning)


@fixture
def rng(request):
    from numpy.random import default_rng

    return default_rng(getattr(request.config.option, "randomly_seed", None))
