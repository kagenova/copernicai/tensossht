import numpy as np
import tensorflow as tf
from pytest import approx, mark


@mark.parametrize("sampling", ["mw", "mwss"])
def test_forward_real_harmonic_layer(sampling, lmax=4, dtype=tf.float64):
    from tensossht import legendre_labels, spherical_harmonics
    from tensossht.layers import ForwardLayer
    from tensossht.sampling import image_sampling_scheme

    labels = legendre_labels(lmax, mmin=0)
    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = spherical_harmonics(grid[0], grid[1], labels=labels)
    rsph = tf.math.real(sph)

    inputs = tf.keras.layers.Input(rsph.shape[:2], dtype=dtype)
    ssht = ForwardLayer(dtype=dtype, is_real=True, sampling=sampling)(inputs)
    model = tf.keras.models.Model(inputs=inputs, outputs=ssht)
    expected = tf.eye(sph.shape[-1], dtype=tf.int32) / tf.where(labels[1] == 0, 1, 2)
    y = model(tf.transpose(rsph, [2, 0, 1]))
    assert y.numpy() == approx(expected.numpy(), abs=1e-7)


@mark.parametrize("spin", (0, 1, 2))
@mark.parametrize("sampling", ["mw", "mwss"])
def test_forward_complex_harmonic_layer(sampling, spin, lmax=4, dtype=tf.float64):
    from tensossht.layers import ForwardLayer
    from tensossht.sampling import image_sampling_scheme
    from tensossht.specialfunctions.naive import spin_spherical_harmonics

    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = spin_spherical_harmonics(
        grid[0], grid[1], lmax=lmax, mmin=None, smin=spin, smax=spin
    )

    cdtype = {tf.float32: tf.complex64, tf.float64: tf.complex128}[dtype]
    inputs = tf.keras.layers.Input(sph.shape[:2], dtype=cdtype)
    ssht = ForwardLayer(dtype=cdtype, is_real=False, spin=spin, sampling=sampling)(
        inputs
    )
    model = tf.keras.models.Model(inputs=inputs, outputs=ssht)
    y = model(tf.transpose(sph, [2, 0, 1]))
    assert y.numpy() == approx(np.eye(sph.shape[-1]), abs=1e-7)


@mark.parametrize("sampling", ["mw", "mwss"])
def test_inverse_real_harmonic_layer(sampling, lmax=4, dtype=tf.float64):
    from tensossht import legendre_labels, spherical_harmonics
    from tensossht.layers import InverseLayer
    from tensossht.sampling import image_sampling_scheme

    labels = legendre_labels(lmax, mmin=0)
    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = tf.transpose(spherical_harmonics(grid[0], grid[1], labels=labels), [2, 0, 1])

    cdtype = {tf.float32: tf.complex64, tf.float64: tf.complex128}[dtype]
    coeffs = tf.eye(sph.shape[0], dtype=cdtype) / tf.cast(
        tf.where(labels[1] == 0, 1, 2), cdtype
    )
    inputs = tf.keras.layers.Input(coeffs.shape[1:], dtype=cdtype)
    ssht = InverseLayer(dtype=dtype, is_real=True, sampling=sampling)(inputs)
    model = tf.keras.models.Model(inputs=inputs, outputs=ssht)
    y = model(coeffs)
    assert y.dtype == dtype
    assert y.numpy() == approx(tf.math.real(sph).numpy(), abs=1e-6)


@mark.parametrize("spin", (0, 1, 2))
@mark.parametrize("sampling", ["mw", "mwss"])
def test_inverse_complex_harmonic_layer(sampling, spin, lmax=4, dtype=tf.float64):
    from tensossht.layers import InverseLayer
    from tensossht.sampling import image_sampling_scheme
    from tensossht.specialfunctions.naive import spin_spherical_harmonics

    grid = image_sampling_scheme(sampling).value(lmax, dtype=dtype).grid
    sph = tf.transpose(
        spin_spherical_harmonics(
            grid[0], grid[1], lmax=lmax, smin=spin, smax=spin, mmin=-lmax
        ),
        [2, 0, 1],
    )

    cdtype = {tf.float32: tf.complex64, tf.float64: tf.complex128}[dtype]
    coeffs = tf.eye(sph.shape[0], dtype=cdtype)
    inputs = tf.keras.layers.Input(coeffs.shape[1:], dtype=cdtype)
    ssht = InverseLayer(dtype=dtype, is_real=False, spin=spin, sampling=sampling)(
        inputs
    )
    model = tf.keras.models.Model(inputs=inputs, outputs=ssht)
    y = model(coeffs)
    assert y.dtype == cdtype
    assert y.numpy() == approx(sph.numpy(), abs=1e-6)


@mark.parametrize("sampling,is_real,dtype", [("mw", True, "float64")])
def test_serialize_model(
    sampling, is_real, tmp_path, dtype, theta_dim=1, phi_dim=2, lmax=5
):
    from tensossht.layers import ForwardLayer
    from tensossht.sampling import image_sampling_scheme

    inputs = tf.keras.layers.Input(
        (*image_sampling_scheme(sampling).value(lmax).shape, 10), dtype=dtype
    )
    ssht = ForwardLayer(
        dtype=dtype,
        is_real=is_real,
        sampling=sampling,
        theta_dim=theta_dim,
        phi_dim=phi_dim,
    )(inputs)
    model = tf.keras.models.Model(inputs=inputs, outputs=ssht)
    model.save(str(tmp_path / "model"))
    reloaded = tf.keras.models.load_model(str(tmp_path / "model"))
    config = reloaded.layers[-1].get_config()
    assert config["is_real"] == is_real
    assert config["sampling"].upper() == sampling.upper()
    assert config["dtype"] == dtype


@mark.parametrize("is_real", [True, False])
def test_packing_layer(is_real, lmax=5):
    from tensossht.layers import PackingLayer
    from tensossht.sampling import legendre_labels

    mmin = 0 if is_real else (1 - lmax)
    coefficients = tf.constant(
        [
            [
                [order if order >= abs(m) else 100 for m in range(mmin, lmax)]
                for order in range(lmax)
            ],
            [
                [m if order >= abs(m) else 100 for m in range(mmin, lmax)]
                for order in range(lmax)
            ],
        ]
    )

    inputs = tf.keras.layers.Input(coefficients.shape[1:], dtype=coefficients.dtype)
    packing = PackingLayer(is_real=is_real, dtype=coefficients.dtype)(inputs)

    model = tf.keras.models.Model(inputs=inputs, outputs=packing)
    y = model(coefficients)

    assert y.shape == (2, (((lmax * (lmax + 1)) // 2) if is_real else (lmax * lmax)))
    assert y.numpy() == approx(legendre_labels(lmax=lmax, mmin=mmin).numpy())


@mark.parametrize("is_real", [True, False])
def test_unpacking_layer(is_real, lmax=5):
    from tensossht.layers import UnpackingLayer
    from tensossht.sampling import legendre_labels

    mmin = 0 if is_real else (1 - lmax)
    coefficients = legendre_labels(lmax=lmax, mmin=mmin)
    inputs = tf.keras.layers.Input(coefficients.shape[1:], dtype=coefficients.dtype)
    packing = UnpackingLayer(
        is_real=is_real, dtype=coefficients.dtype, fill_value=100, l_dim=-2, m_dim=-1
    )(inputs)

    model = tf.keras.models.Model(inputs=inputs, outputs=packing)
    y = model(coefficients)

    expected = [
        [
            [order if order >= abs(m) else 100 for m in range(mmin, lmax)]
            for order in range(lmax)
        ],
        [
            [m if order >= abs(m) else 100 for m in range(mmin, lmax)]
            for order in range(lmax)
        ],
    ]

    assert y.shape == (2, lmax, lmax - mmin)
    assert y.numpy() == approx(np.array(expected))


@mark.parametrize("is_odd_spin", (True, False))
def test_real_fft_layer(is_odd_spin: bool):
    from tensossht.layers.wigner import FourierLayer

    shape = (5 + int(not is_odd_spin), 28)
    inputs = tf.keras.layers.Input(shape, dtype=tf.float64)
    fft = FourierLayer(is_forward=True, is_real=True, axis=-2, dtype=tf.float64)(inputs)
    ifft = FourierLayer(
        is_forward=False,
        is_real=True,
        is_odd_spin=is_odd_spin,
        dtype=tf.float64,
        out_axis=-2,
    )(fft)

    model = tf.keras.models.Model(inputs=inputs, outputs=ifft)

    initial = tf.random.uniform((2, *shape[::-1]), dtype=tf.float64)
    rsignal = tf.transpose(
        tf.signal.irfft(tf.signal.rfft(initial), fft_length=shape[:1]), [0, 2, 1]
    )
    assert (model(tf.transpose(initial, [0, 2, 1])) == rsignal).numpy().all()
    assert not (tf.transpose(initial, [0, 2, 1]) == rsignal).numpy().all()


def test_real_wigner_layer(sampling="mw", lmax=10, ngamma=9):
    from tensossht.layers import wigner_layer
    from tensossht.sampling import image_sampling_scheme

    sampling = image_sampling_scheme(sampling).value(lmax)

    cinputs = tf.keras.layers.Input((*sampling.shape, ngamma), dtype=tf.complex128)
    cforward = wigner_layer(is_forward=True, sampling=sampling, dtype=tf.complex128)(
        cinputs
    )
    cmodel = tf.keras.models.Model(inputs=cinputs, outputs=cforward)

    rinputs = tf.keras.layers.Input((*sampling.shape, ngamma), dtype=tf.float64)
    rforward = wigner_layer(
        is_forward=True, sampling=sampling, dtype=tf.float64, is_real=True
    )(rinputs)
    rmodel = tf.keras.models.Model(inputs=rinputs, outputs=rforward)

    x = tf.cast(tf.random.uniform((2, *sampling.shape, ngamma)), dtype=tf.complex128)
    y = rmodel(tf.math.real(x))
    assert y.numpy() == approx(cmodel(x)[:, ngamma // 2 :].numpy())

    inv_inputs = tf.keras.layers.Input(
        (ngamma // 2 + 1, lmax * lmax), dtype=tf.complex128
    )
    inv_layer = wigner_layer(
        is_forward=False, sampling=sampling, dtype=tf.complex128, is_real=True
    )(inv_inputs)
    inv_model = tf.keras.models.Model(inputs=inv_inputs, outputs=inv_layer)
    xback = inv_model(y)

    cinv_inputs = tf.keras.layers.Input((ngamma, lmax * lmax), dtype=tf.complex128)
    cinv_layer = wigner_layer(
        is_forward=False, sampling=sampling, dtype=tf.complex128, is_real=False
    )(cinv_inputs)
    cinv_model = tf.keras.models.Model(inputs=cinv_inputs, outputs=cinv_layer)
    cxback = cinv_model(cmodel(x))
    assert xback.numpy() == approx(tf.math.real(cxback).numpy())


@mark.parametrize(
    "kwargs,output",
    [
        (dict(is_forward=True, is_real=False, axis=-1, out_axis=-1), [None, 4, 5, 6]),
        (dict(is_forward=True, is_real=False, axis=-2, out_axis=-1), [None, 4, 6, 5]),
        (dict(is_forward=True, is_real=False, axis=-1, out_axis=-2), [None, 4, 6, 5]),
        (dict(is_forward=True, is_real=True, axis=-1, out_axis=-1), [None, 4, 5, 4]),
        (dict(is_forward=True, is_real=True, axis=-2, out_axis=-2), [None, 4, 3, 6]),
        (dict(is_forward=False, is_real=False, axis=-1, out_axis=-1), [None, 4, 5, 6]),
        (dict(is_forward=False, is_real=False, axis=-2, out_axis=-1), [None, 4, 6, 5]),
        (dict(is_forward=False, is_real=False, axis=-1, out_axis=-2), [None, 4, 6, 5]),
        (
            dict(
                is_forward=False, is_real=True, is_odd_spin=True, axis=-2, out_axis=-1
            ),
            [None, 4, 6, 9],
        ),
        (
            dict(
                is_forward=False, is_real=True, is_odd_spin=False, axis=-2, out_axis=-1
            ),
            [None, 4, 6, 8],
        ),
    ],
)
def test_compute_fourier_output_shape(kwargs, output, shape=[None, 4, 5, 6]):
    from tensossht.layers.wigner import FourierLayer

    actual = FourierLayer(**kwargs).compute_output_shape(shape)
    assert list(actual) == output


@mark.parametrize("nspins", [5, 1])
@mark.parametrize("spin, phi, theta", [(-3, -2, -1), (-1, -3, -2)])
def test_complex_inverse_harmonic_compute_shape(
    spin, phi, theta, nspins, sampling="mw", lmax=10
):
    from tensossht.layers import InverseSpinLayer
    from tensossht.sampling import image_sampling_scheme

    sampling = image_sampling_scheme(sampling).value(lmax)

    inputs = tf.keras.layers.Input((nspins, lmax * lmax), dtype=tf.complex128)
    ssht = InverseSpinLayer(
        sampling=sampling,
        dtype=tf.complex128,
        out_spin_dim=spin,
        theta_dim=theta,
        phi_dim=phi,
    )
    shape = ssht.compute_output_shape(inputs.shape)
    assert shape[spin] == nspins
    assert shape[theta] == len(sampling.thetas)
    assert shape[phi] == len(sampling.phis)
