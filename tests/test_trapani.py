from typing import cast

import numpy as np
import tensorflow as tf
from pytest import approx, mark

from tensossht.typing import TFArray


@mark.parametrize("l", [0, 1, 4, 10])
def test_nbcoeffs(l):
    from tensossht.specialfunctions.trapani import ncoeffs

    expected = 0
    for m2 in range(0, l + 1):
        for _ in range(0, m2 + 1):
            expected += 1
    assert ncoeffs(l) == expected


@mark.parametrize("l", [0, 1, 4, 10])
def test_index(l):
    from tensossht.specialfunctions.trapani import linear_index

    i = 0
    for m1 in range(0, l + 1):
        for m2 in range(0, m1 + 1):
            assert linear_index(m1, m2) == i
            i += 1


def test_outer_recursion_tensor():
    from tensossht.specialfunctions.trapani import linear_index as i
    from tensossht.specialfunctions.trapani import outer_recursion_tensor

    L1 = outer_recursion_tensor(1, dtype="float64")
    assert L1.shape == (3, 1)
    assert L1.dtype == "float64"
    assert float(L1[i(0, 0), 0]) == approx(0)
    assert float(L1[i(1, 0), 0]) == -np.sqrt(0.5)
    assert float(L1[i(1, 1), 0]) == 0.5

    L2 = outer_recursion_tensor(2, dtype="float64")
    assert L2.shape == (6, 3)
    assert float(L2[i(2, 0), i(1, 0)]) == approx(-np.sqrt(3) / 2)
    for m2 in range(1, 3):
        assert float(L2[i(2, m2), i(1, m2 - 1)]) == approx(
            np.sqrt(3 / ((2 + m2) * (1 + m2)))
        )
    for m1 in range(2):
        for m2 in range(m1 + 1):
            assert np.array(L2[i(m1, m2), :]) == approx(0.0)


def test_inner_recursion_tensor():
    from tensossht.specialfunctions.trapani import inner_recursion_tensor

    L1 = inner_recursion_tensor(1, dtype="float64")
    assert L1.shape == (3, 3)
    assert L1.dtype == "float64"
    assert np.array(L1) == approx(np.identity(3))

    L2 = inner_recursion_tensor(2, dtype="float64")
    assert L2.shape == (6, 6)


def test_outer_recursion_sparse():
    from numpy import array

    from tensossht.specialfunctions.trapani import (
        outer_recursion_sparse,
        outer_recursion_tensor,
    )

    for l in range(1, 7):
        dense = array(outer_recursion_tensor(l))
        assert array(tf.sparse.to_dense(outer_recursion_sparse(l))) == approx(dense)


def test_inner_recursion_sparse():
    from numpy import array

    from tensossht.specialfunctions.trapani import (
        inner_recursion_sparse,
        inner_recursion_tensor,
    )

    for l in range(1, 7):
        dense = array(inner_recursion_tensor(l))
        assert array(tf.sparse.to_dense(inner_recursion_sparse(l))) == approx(dense)


def test_recursion():
    from tensossht.specialfunctions.naive import wignerd as naive
    from tensossht.specialfunctions.trapani import initial, recursion_tensor

    x = initial()
    for l in range(1, 8):
        x = recursion_tensor(l) @ x
        indices = [(l, m1, m2) for m1 in range(l + 1) for m2 in range(m1 + 1)]
        expected = [naive(*index, precision=512) for index in indices]
        assert np.array(x).squeeze() == approx(np.array(expected, dtype=float))


def test_fulltensor():
    from functools import reduce

    from tensossht.specialfunctions.naive import wignerd as naive
    from tensossht.specialfunctions.trapani import full_tensor
    from tensossht.specialfunctions.trapani import recursion_tensor as R

    L = 5
    compressed = cast(TFArray, reduce(tf.matmul, [R(i) for i in range(L, 0, -1)]))
    expected = np.array(
        [[naive(L, m1, m2) for m2 in range(-L, L + 1)] for m1 in range(-L, L + 1)],
        dtype=float,
    )
    actual = full_tensor(compressed)
    assert np.array(actual, dtype=float) == approx(expected)


@mark.parametrize("l", list(range(0, 5)))
def test_straightforward(l):
    from tensossht.specialfunctions.naive import wignerd as naive
    from tensossht.specialfunctions.trapani import straightforward

    assert straightforward(l, -l - 1, 0) == 0
    assert straightforward(l, 0, -l - 1) == 0
    assert straightforward(l, l + 1, 0) == 0
    assert straightforward(l, 0, l + 1) == 0

    for m1 in range(-l, l + 1):
        for m2 in range(-l, l + 1):
            assert straightforward(l, m1, m2) == approx(float(naive(l, m1, m2)))


def test_invalid_labels():
    from tensossht.specialfunctions.trapani import deltas

    result = deltas(labels=np.array([[0, 0, 0, 0], [0, 1, -1, 1], [0, -1, 1, 1]]))
    assert np.array(result[1:]) == approx(0)
    assert result[0].numpy() == approx(1)
