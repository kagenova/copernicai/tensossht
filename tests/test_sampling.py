import numpy as np
from pytest import approx, mark, raises

from tensossht import sampling


def test_equiangular_theta():
    for lmax in [1, 5, 10]:
        expected = [(2 * t + 1) * np.pi / (2 * lmax - 1) for t in range(lmax)]
        assert sampling.equiangular_theta(lmax).numpy() == approx(expected)


def test_symmetric_theta():
    for lmax in [1, 5, 10]:
        expected = [t * np.pi / lmax for t in range(lmax + 1)]
        assert sampling.symmetric_theta(lmax).numpy() == approx(expected)


def test_equiangular_phi():
    for lmax in [1, 5, 10]:
        expected = [(2 * np.pi * p) / (2 * lmax - 1) for p in range(2 * lmax - 1)]
        assert sampling.equiangular_phi(lmax).numpy() == approx(expected)


def test_symmetric_phi():
    for lmax in [1, 5, 10]:
        expected = [np.pi * p / lmax for p in range(2 * lmax)]
        assert sampling.symmetric_phi(lmax).numpy() == approx(expected)


def test_equiangular_shape():
    assert len(sampling.equiangular_theta(5)) == sampling.equiangular_shape(5)[0]
    assert len(sampling.equiangular_theta(5)) == sampling.equiangular_shape(5).theta
    assert len(sampling.equiangular_phi(5)) == sampling.equiangular_shape(5)[1]
    assert len(sampling.equiangular_phi(5)) == sampling.equiangular_shape(5).phi


def test_equiangular():
    actual = sampling.equiangular(3).numpy().round(2)
    expected = np.array(
        [
            [
                [0.63, 0.63, 0.63, 0.63, 0.63],
                [1.88, 1.88, 1.88, 1.88, 1.88],
                [3.14, 3.14, 3.14, 3.14, 3.14],
            ],
            [
                [0.0, 1.26, 2.51, 3.77, 5.03],
                [0.0, 1.26, 2.51, 3.77, 5.03],
                [0.0, 1.26, 2.51, 3.77, 5.03],
            ],
        ]
    )
    assert actual == approx(expected)


@mark.parametrize(
    "operation",
    [
        sampling.equiangular_theta,
        sampling.equiangular_phi,
        sampling.symmetric_phi,
        sampling.symmetric_theta,
    ],
)
def test_lmax_too_small(operation):
    with raises(ValueError):
        operation(0)
