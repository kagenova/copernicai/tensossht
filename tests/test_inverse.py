from typing import Callable, Optional, Type, cast

import numpy as np
import tensorflow as tf
from pytest import approx, mark, skip

from tensossht.transforms.coeffs_to_fmm import CoeffToFmmLSummation, CoeffToFmmSummation
from tensossht.typing import TFArray


@mark.parametrize("SummationClass", (CoeffToFmmSummation, CoeffToFmmLSummation))
@mark.parametrize("spin", [0, -1, 2])
@mark.parametrize("mmin", [None, 0])
def test_flm_to_Fmm_coeffsum_mmin(
    mmin: Optional[int], spin: int, SummationClass: Type, lmax: int = 8
):
    from tensossht.sampling import spin_legendre_labels
    from tensossht.specialfunctions.naive import wignerd

    if mmin is None:
        mmin = -lmax + 1
    if SummationClass is CoeffToFmmLSummation and (mmin != -lmax + 1 or spin != 0):
        skip("Not implemented")
    labels = spin_legendre_labels(lmax, mmin=mmin, smin=spin, smax=spin)
    coeffsum = SummationClass.factory(labels, dtype=tf.float64)
    dtype = tf.float64
    coefficients = tf.complex(
        tf.random.uniform((labels.shape[1], 3), dtype=dtype),
        tf.random.uniform((labels.shape[1], 3), dtype=dtype),
    )
    Fmm = coeffsum(tf.transpose(coefficients))

    lcoeffs = tf.RaggedTensor.from_value_rowids(coefficients, labels[0]).to_tensor()
    for mp in range(mmin, lmax):
        summands = np.array(
            [
                [
                    float(wignerd(degree, mp, m) * wignerd(degree, mp, -spin))
                    * np.sqrt((2 * degree + 1) / (4 * np.pi))
                    * lcoeffs[degree, max((m + degree) if mmin < 0 else m, 0)].numpy()
                    * 1j ** -(m + spin)
                    * (-1) ** spin
                    * (2 * lmax - 1) ** 2
                    for m in range(mmin, lmax)
                ]
                for degree in range(lmax)
            ]
        )
        expected = np.sum(summands, axis=0)
        index = mp if mp >= 0 else 2 * lmax - 1 + mp
        assert Fmm[index].numpy() == approx(expected.T, abs=1e-4, rel=1e-5)


@mark.parametrize("SummationClass", (CoeffToFmmSummation, CoeffToFmmLSummation))
def test_flm_to_Fmm_coeffsum_multispin(
    SummationClass: Type, spin=2, lmax=8, dtype=tf.float64
):
    from tensossht.sampling import spin_legendre_labels
    from tensossht.specialfunctions.naive import wignerd

    labels = spin_legendre_labels(
        lmax, mmin=None, smin=-spin, smax=spin, compact_spin=False
    )
    coefficients = tf.complex(
        tf.random.uniform((labels.shape[1], 3), dtype=dtype),
        tf.random.uniform((labels.shape[1], 3), dtype=dtype),
    )
    coefficients = tf.where(
        (labels[0] >= tf.math.abs(labels[2]))[:, None], coefficients, 0
    )
    coefficients = tf.reshape(coefficients, (-1, 2 * spin + 1, coefficients.shape[-1]))
    coeffsum = SummationClass.factory(labels, dtype=tf.float64)
    Fmm = coeffsum(tf.transpose(coefficients))

    lcoeffs = tf.RaggedTensor.from_value_rowids(
        coefficients, labels[0, : coefficients.shape[0]]
    ).to_tensor()

    for s in range(-spin, spin):
        for mp in range(1 - lmax, lmax):
            summands = np.array(
                [
                    [
                        float(wignerd(degree, mp, m) * wignerd(degree, mp, -s))
                        * np.sqrt((2 * degree + 1) / (4 * np.pi))
                        * np.array(lcoeffs[degree, max(m + degree, 0), s + spin])
                        * 1j ** -(m + s)
                        * (-1) ** s
                        * (2 * lmax - 1) ** 2
                        for m in range(-lmax + 1, lmax)
                    ]
                    for degree in range(lmax)
                ]
            )
            expected = np.sum(summands, axis=0)
            index = mp if mp >= 0 else 2 * lmax - 1 + mp
            assert np.array(Fmm[index, :, s + spin]) == approx(
                expected.T, abs=1e-4, rel=1e-5
            )


@mark.parametrize("sampling,f00", [("mw", 22.84967782), ("mwss", 28.209478784887686)])
def test_coefficients_to_fmm_real(sampling, f00, lmax=5, dtype=tf.complex128):
    from tensossht.sampling import legendre_labels
    from tensossht.transforms.coeffs_to_fmm import coeffs_to_fmm

    labels = legendre_labels(lmax, mmin=0)
    ctof = coeffs_to_fmm(labels, sampling, dtype=dtype)

    fmm = ctof(tf.eye(labels.shape[1], dtype=dtype)[:2])
    assert fmm[0, 0, 0].numpy() == approx(f00)
    assert tf.reduce_sum(tf.abs(fmm[0])).numpy() == approx(f00)

    f10 = fmm[1, 1, 0].numpy()
    assert fmm[1, -1, 0].numpy() == approx(f10.conj())
    assert tf.reduce_sum(tf.abs(fmm[1])).numpy() == approx(2 * abs(f10))


@mark.parametrize("sampling,f00", [("mw", 22.84967782), ("mwss", 28.209478784887686)])
def test_coefficients_to_fmm_complex(sampling, f00, lmax=5, dtype=tf.complex128):
    from tensossht.sampling import legendre_labels
    from tensossht.transforms.coeffs_to_fmm import coeffs_to_fmm

    labels = legendre_labels(lmax, mmin=None)
    ctof = cast(
        Callable[[TFArray], TFArray], coeffs_to_fmm(labels, sampling, dtype=dtype)
    )

    fmm = ctof(tf.eye(labels.shape[1], dtype=dtype)[:2])
    index = (lmax - 1) if sampling == "mw" else lmax
    assert fmm[0, 0, index].numpy() == approx(f00)
    assert tf.reduce_sum(tf.abs(fmm[0])).numpy() == approx(f00)

    index = (lmax - 2) if sampling == "mw" else (lmax - 1)
    f10 = fmm[1, 1, index].numpy()
    assert fmm[1, -1, index].numpy() == approx(f10.conj())
    assert tf.reduce_sum(tf.abs(fmm[1])).numpy() == approx(2 * abs(f10))


@mark.parametrize("spin", (0, 1, 2, 3, 4))
@mark.parametrize("sampling", ["mw", "mwss"])
def test_inverse_complex(sampling, spin, lmax=5, dtype=tf.complex128):
    from tensossht.sampling import image_sampling_scheme
    from tensossht.specialfunctions.naive import spin_spherical_harmonics
    from tensossht.transforms.inverse import inverse_transform

    transform = cast(
        Callable[[TFArray], TFArray],
        inverse_transform(lmax, mmin=-lmax, spin=spin, sampling=sampling, dtype=dtype),
    )

    grid = image_sampling_scheme(sampling).value(lmax, dtype=tf.float64).grid
    sph = tf.transpose(
        spin_spherical_harmonics(
            grid[0], grid[1], lmax=lmax, mmin=-lmax, smin=spin, smax=spin
        ),
        [2, 0, 1],
    )

    real_space = transform(tf.eye(sph.shape[0], dtype=dtype))
    assert np.array(real_space) == approx(sph.numpy(), abs=1e-6)


@mark.parametrize("sampling", ["mw", "mwss"])
def test_inverse_real(sampling, lmax=5, dtype=tf.complex128):
    from tensossht.sampling import image_sampling_scheme, legendre_labels
    from tensossht.specialfunctions import spherical_harmonics
    from tensossht.transforms.inverse import inverse_transform

    labels = legendre_labels(lmax, mmin=0)
    transform = cast(
        Callable[[TFArray], TFArray],
        inverse_transform(labels=labels, sampling=sampling, dtype=dtype),
    )
    real_space = transform(
        tf.eye((lmax * (lmax + 1)) // 2, dtype=dtype)
        * tf.cast(tf.where(labels[1] == 0, 1.0, 0.5), dtype=dtype)
    )

    grid = image_sampling_scheme(sampling).value(lmax, dtype=tf.float64).grid
    sph = tf.transpose(
        spherical_harmonics(grid[0], grid[1], lmax=lmax, mmin=0), [2, 0, 1]
    )

    assert np.array(real_space) == approx(np.array(tf.math.real(sph)), abs=1e-6)


@mark.parametrize("sampling", ["mw", "mwss"])
def test_inverse_complex_spin(sampling, spin=2, lmax=5, dtype=tf.complex128):
    from tensossht.sampling import harmonic_sampling_scheme, image_sampling_scheme
    from tensossht.specialfunctions.naive import spin_spherical_harmonics
    from tensossht.transforms.inverse import inverse_transform

    hsampling = harmonic_sampling_scheme(lmax, mmin=-lmax, smin=-spin, smax=spin)
    transform = inverse_transform(hsampling, sampling=sampling, dtype=dtype)

    grid = image_sampling_scheme(sampling).value(lmax, dtype=tf.float64).grid

    ncoeffs = hsampling.ncoeffs // hsampling.nspins
    coeffs = tf.reshape(
        tf.eye(hsampling.ncoeffs, dtype=dtype),
        (hsampling.nspins, ncoeffs, hsampling.nspins, ncoeffs),
    )
    real_space = transform(coeffs)

    for sin in range(-spin, spin + 1):
        for sout in range(-spin, spin + 1):
            element = np.array(real_space[sin + spin, :, sout + spin])
            if sin != sout:
                assert element == approx(0)
            else:
                sph = spin_spherical_harmonics(
                    grid[0],
                    grid[1],
                    lmax=lmax,
                    mmin=-lmax,
                    smin=sin,
                    smax=sin,
                    compact_spin=False,
                )
                sph = tf.transpose(sph, [2, 0, 1]).numpy()
                assert element == approx(sph, rel=1e-6, abs=1e-6)


def test_single_vs_many_spin_factor(lmax=5, smin=-3, smax=3, dtype=tf.float64):
    from tensossht.sampling import harmonic_sampling_scheme
    from tensossht.transforms.coeffs_to_fmm import many_spin_factor, single_spin_factor

    many = many_spin_factor(
        harmonic_sampling_scheme(lmax=lmax, smin=smin, smax=smax), dtype=dtype
    )
    for s in range(smin, smax + 1):
        single = single_spin_factor(
            harmonic_sampling_scheme(lmax=lmax, smin=s, smax=s), dtype=dtype
        )
        assert many[s - smin].numpy() == approx(np.array(single))


def test_legendre_lsum_with_one_in_shape():
    from tensossht.sampling import legendre_labels
    from tensossht.specialfunctions import legendre_lsum

    lmax, lmin, mmax, mmin = 8, 2, 4, -3
    labels = legendre_labels(lmax=lmax, lmin=lmin, mmax=mmax, mmin=mmin)
    result = np.array(
        legendre_lsum(
            tf.reshape(labels[0], (1, 1, labels.shape[1])),
            lmax=lmax,
            lmin=lmin,
            mmax=mmax,
            mmin=mmin,
            axis=-1,
        )
    )
    expected = np.array([25, 27, 27, 27, 27, 27, 25, 22], dtype=int)[None, None, :]
    assert result.shape == expected.shape
    assert result == approx(expected)
