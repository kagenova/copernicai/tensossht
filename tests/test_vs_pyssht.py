import numpy as np
import tensorflow as tf
from pytest import approx, mark


@mark.parametrize("spin", list(range(4)))
@mark.parametrize("sampling", ["mw", "mwss"])
def test_complex_forward(
    sampling: str, spin: int, rng: np.random.Generator, lmax=5, dtype=tf.float64
):
    from pyssht import forward as ctransform  # type: ignore

    from tensossht.sampling import image_sampling_scheme
    from tensossht.transforms.forward import forward_transform

    transform = forward_transform(lmax, dtype=dtype, sampling=sampling, spin=spin)
    grid = image_sampling_scheme(sampling).value(lmax).shape
    image = rng.uniform(size=(*grid, 2)) @ [1, 1j]

    py_coeffs = transform(tf.convert_to_tensor(image), theta_dim=0, phi_dim=1).numpy()
    c_coeffs = ctransform(
        image, lmax, Spin=spin, Reality=False, Method=sampling.upper()
    )
    assert py_coeffs == approx(c_coeffs[spin * spin :])


@mark.parametrize("sampling", ["mw", "mwss"])
def test_real_forward(
    sampling: str, rng: np.random.Generator, lmax=5, dtype=tf.float64
):
    from pyssht import forward as ctransform  # type: ignore

    from tensossht.sampling import image_sampling_scheme
    from tensossht.transforms.forward import forward_transform

    transform = forward_transform(lmax, mmin=0, spin=0, dtype=dtype, sampling=sampling)
    grid = image_sampling_scheme(sampling).value(lmax).shape
    image = rng.uniform(size=grid)

    py_coeffs = transform(tf.convert_to_tensor(image), theta_dim=0, phi_dim=1).numpy()
    c_coeffs = ctransform(image, lmax, Reality=True, Method=sampling.upper())
    indices = [l * l + m + l for l in range(lmax) for m in range(-l, l + 1) if m >= 0]
    assert py_coeffs == approx(c_coeffs[indices])


@mark.parametrize("spin", list(range(4)))
@mark.parametrize("sampling", ["mw", "mwss"])
def test_complex_inverse(
    sampling: str, spin: int, rng: np.random.Generator, lmax=5, dtype=tf.float64
):
    from pyssht import inverse as ctransform  # type: ignore

    from tensossht.transforms.inverse import inverse_transform

    transform = inverse_transform(lmax, dtype=dtype, sampling=sampling, spin=spin)
    coeffs = rng.uniform(size=(lmax * lmax, 2)) @ [1, 1j]

    py_image = transform(tf.convert_to_tensor(coeffs[spin * spin :])).numpy()
    c_image = ctransform(
        coeffs, lmax, Spin=spin, Reality=False, Method=sampling.upper()
    )
    assert py_image == approx(c_image)


@mark.parametrize("sampling", ["mw", "mwss"])
def test_real_inverse(
    sampling: str, rng: np.random.Generator, lmax=5, dtype=tf.float64
):
    from pyssht import inverse as ctransform  # type: ignore

    from tensossht.transforms.inverse import inverse_transform

    transform = inverse_transform(lmax, dtype=dtype, sampling=sampling, spin=0, mmin=0)
    coeffs = rng.uniform(size=(lmax * lmax, 2)) @ [1, 1j]

    indices = [l * l + m + l for l in range(lmax) for m in range(-l, l + 1) if m >= 0]
    py_image = transform(tf.convert_to_tensor(coeffs[indices])).numpy()
    c_image = ctransform(coeffs, lmax, Spin=0, Reality=True, Method=sampling.upper())
    assert py_image == approx(c_image)
