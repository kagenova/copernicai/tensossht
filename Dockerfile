FROM tensorflow/tensorflow:2.2.0-gpu
RUN apt-get update \
    && apt-get install -y python3-venv git \
    && rm -rf /var/lib/apt/lists/*
RUN python -m venv /venv/ --system-site-packages \
    && . /venv/bin/activate  \
    && pip install --upgrade pip \
    && pip install poetry
ENTRYPOINT ["/bin/bash"]
