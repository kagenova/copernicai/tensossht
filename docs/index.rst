.. TensoSSHT documentation master file, created by
   sphinx-quickstart on Sat Jun 27 10:45:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TensoSSHT's documentation!
=====================================

TensoSSHT is a fast and exact harmonic transform for `Tensorflow
<https://tensorflow.org>`__.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   layers
   transforms
   sampling
   Special Functions <specialfunctions>
   misc


Installation
============

TensoSSHT is released on [pypi](https://pypi.org/project/tensossht/) and can be
installed via `pip`:

.. code-block:: console

    > pip install tensossht

It is also possible to install it from source. TensoSSHT is developed with
[poetry](https://python-poetry.org/).
and run:

.. code-block:: console

    > git clone https://gitlab.com/kagenova/copernicai/tensossht
    > cd tensossht
    > poetry install --with=docs
    > poetry run pytest  # Optional, all tests should pass :)
    > poetry run python

`--with=docs` also installs the dependencies for building the documentation and is not
strictly necessary otherwise.

A Note about Performance
========================

As of tensorflow 2.11, we've had varying success running the forward and inverse
transforms depending on hardware. On CPUs the inverse and forward transform perform more
or less the same, with the latter having a slight edge. On GPUs however, the inverse
transform can be quite a bit slower, especially at large L.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
